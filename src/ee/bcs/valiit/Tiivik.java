package ee.bcs.valiit;

import java.applet.Applet;
import java.awt.*;

public class Tiivik extends Applet {

    private Graphics graph;

    @Override
    public void init() {
        this.setSize(600, 600);
        this.setBackground(Color.white);
    }

    @Override
    public void paint(Graphics g) {
        this.graph = g;
        drawImpeller();

    }

    public void drawBlade(double angle) {
        int cX = getWidth() / 2;
        int cY = getHeight() / 2;
        int r = 250;
        double width = 2 * Math.PI / 20;
        int x = cX + (int) (r * Math.cos(angle));
        int y = cY + (int) (r * Math.sin(angle));
        int bX = cX + (int) (r * Math.cos(angle + width));
        int bY = cY + (int) (r * Math.sin(angle + width));
        graph.drawLine(x, y, bX, bY);
        graph.drawLine(x, y, cX, cY);
        graph.drawLine(cX, cY, bX, bY);
    }
    public void drawImpeller() {
        graph.setColor(Color.RED);
        for (double angle = 0; angle <= Math.PI * 2; angle += Math.PI * 2 / 10)
            drawBlade(angle);
    }

}
